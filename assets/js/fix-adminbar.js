/**
 * FIX - FIXED ADMIN BAR
 * 
 * Prevent wp admin bar overlapping 
 * the fixed header.
 * 
 * @doc awesomatic.nl/support/docs/
 ********************************/

jQuery(document).ready(function($) {
  /**
   * Get the height of the admin bar automatically ..
   */
  var height= $("#wpadminbar").height();
  /**
   * .. and add this height to the margin-top of #top-header
   */
  $("#top-header").css({marginTop: height});
});




// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});
