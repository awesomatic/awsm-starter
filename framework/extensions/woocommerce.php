<?php

/**
 * Show cart contents / total Ajax
 * 
 * @link https://
 */

	function woocommerce_header_add_to_cart_fragment( $fragments ) { 

		global $woocommerce;
		/**
		 * Items count 
		 */
		$cart_contents_count = sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'awsm-starter'), $woocommerce->cart->cart_contents_count); 
		//$cart_contents_count = sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'awsm-starter'), $woocommerce->cart->cart_contents_count); 
		/**
		 * Items total
		 */
		//$cart_total = $woocommerce->cart->get_cart_total();
		
		ob_start(); ?> 

		<a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'awsm-starter'); ?>"><?php echo $cart_contents_count; ?></a>
		<?php $fragments['a.cart-customlocation'] = ob_get_clean();

		return $fragments;

	}

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );


/**
 * DISABLE WOOCOMMERCE STYLESHEETS
 * 
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
 
	function awsm_child_wc_dequeue_styles( $enqueue_styles ) {
		/**
		 * Remove the gloss
		 */
		unset( $enqueue_styles['woocommerce-general'] );
		/**
		 * Remove the layout
		 */
		unset( $enqueue_styles['woocommerce-layout'] );
		/**
		 * Remove the smallscreen optimisation
		 */
		unset( $enqueue_styles['woocommerce-smallscreen'] );

		return $enqueue_styles;
	}

//add_filter( 'woocommerce_enqueue_styles', 'awsm_child_wc_dequeue_styles' );
