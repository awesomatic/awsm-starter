<?php

/**
 * GOOGLE FONTS
 * 
 * Description: Get the Google Fonts to use in your stylesheets
 * 
 * @link https://fonts.google.com/ 
 */

	function awsm_child_enqueue_google_fonts() {
		/**
		 * This font will be used for headings
		 */
		wp_enqueue_style( 'awsm-starter-google-fonts-1', 'https://fonts.googleapis.com/css?family=Arya:400,700', false );
		/**
		 * This font will be used for regular stuff like body text, links, paragraphs, etc.
		 */
		wp_enqueue_style( 'awsm-starter-google-fonts-2', 'https://fonts.googleapis.com/css?family=Poppins:400,700', false );
		/**
		 * Sometimes to create an eyecatching design, you'll need to use a third font. (= not recommended)
		 */
		wp_enqueue_style( 'awsm-starter-google-fonts-3', 'https://fonts.googleapis.com/css?family=Rubik:400,700', false );
	}

add_action( 'wp_enqueue_scripts', 'awsm_child_enqueue_google_fonts' );

/**
* ENQUEUE EDITOR FONTS
*
* To Do - dequeue parent editor styles if child is used
*/

	function awsm_child_enqueue_block_editor_assets() {
		/**
		 * Load the selected Google Font in the Gutenberg Editor
		 */
		wp_enqueue_style( 'awsm-starter-google-fonts', awsm_child_enqueue_google_fonts(), array(), null );
	}

add_action( 'enqueue_block_editor_assets', 'awsm_child_enqueue_block_editor_assets' );
