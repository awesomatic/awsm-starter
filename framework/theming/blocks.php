<?php 

/**
 * GUTENBERG COLOR PALETTE
 */

    function awsm_child_add_color_palette() {
        /**
         * We don't want users screwing up the theme, so:
         */
        add_theme_support('disable-custom-colors');
        /**
         * Instead, add support for a custom color scheme. 
         * Users can fill in their brand colors via a color picker.
         */
        add_theme_support('editor-color-palette', array(
            /*
            * Primary brand color
            */
            array(
                'name'  => __('Primary', 'awsm-starter'),
                'slug'  => 'primary',
                'color' => '#1b63ff', // This is the color that show up in the Gutenberg editor
            ),
            /*
            * Secondary brand color
            */
            array(
                'name'  => __('Secondary', 'awsm-starter'),
                'slug'  => 'secondary',

                // This is the color that show up in the Gutenberg editor
                'color' => 'hsl(162, 70%, 57%)',
            ),
            /*
            * Primary brand color
            */
            array(
                'name'  => __('Dark', 'awsm-starter'),
                'slug'  => 'Dark',

                // This is the color that show up in the Gutenberg editor
                'color' => '#0a192f',
            ),
            /*
            * Secondary brand color
            */
            array(
                'name'  => __('Light', 'awsm-starter'),
                'slug'  => 'Light',

                // This is the color that show up in the Gutenberg editor
                'color' => 'rgb(204, 214, 246)',
            ),
        ));
    }

add_action('after_setup_theme', 'awsm_child_add_color_palette');
