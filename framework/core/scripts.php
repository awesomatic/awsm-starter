<?php

/**
 * ENQUEUE STYLES
 * 
 * Enqueue all the styles thats needed to get the child theme working. 
 * 
 * @link https://developer.wordpress.org/themes/basics/including-css-javascript/
 */

	function enqueue_styles() {
		/** 
		 * This is 'awsm-style' for the Awesomatic theme. 
		 */
		$parent_style = 'awsm-style'; 
		/**
		 * Enqueue the parent style 
		 */
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
		/**
		 * Enqueue the child style after the parent style and give it a version 
		 */
		wp_enqueue_style( 'awsm-starter-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );
	}

add_action( 'wp_enqueue_scripts', 'enqueue_styles' );


/**
 * ENQUEUE SCRIPTS
 * 
 * wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer);
 * 
 * $handle is the name for the script. 
 * $src defines where the script is located.
 * $deps is an array that can handle any script that your new script depends on, such as jQuery.
 * $ver lets you list a version number.
 * $in_footer is a boolean parameter (true/false (=header)) that allows you to place your scripts in the footer
 * 
 * @link https://developer.wordpress.org/themes/basics/including-css-javascript/
 */

	function enqueue_scripts() {
		/**
		 * Fix - Admin Bar 
		 */
		wp_enqueue_script( 'fix-adminbar', get_stylesheet_directory_uri() . '/assets/js/fix-adminbar.js', array(), '1.0.0', true );
		/**
		 * jQuery - Toggle Hamburger
		 */
		wp_enqueue_script( 'toggle-hamburger', get_stylesheet_directory_uri() . '/assets/js/toggle-hamburger.js', array(), '1.0.0', true );
		/**
		 * jQuery - Elastic Header
		 */
		wp_enqueue_script( 'header-elastic', get_stylesheet_directory_uri() . '/assets/js/elastic-header.js', array(), '1.0.0', true );
		/**
		 * jQuery - Smooth Scroll
		 */
		wp_enqueue_script( 'smooth-scroll', get_stylesheet_directory_uri() . '/assets/js/smooth-scroll.js', array(), '1.0.0', true );
	}

add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
