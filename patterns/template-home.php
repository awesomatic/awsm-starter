<?php
/**
 * Template Name: Homepage
 */

get_header(); ?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

		get_template_part( 'patterns/02-organisms/00-global/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #primary -->

	<?php get_sidebar( 'shop' ); ?>

<?php
get_footer();
