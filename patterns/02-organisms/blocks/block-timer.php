<?php
/**
 * Team Member block
 *
 * @package      ClientName
 * @author       Bill Erickson
 * @since        1.0.0
**/
?>

<!-- https://codepen.io/AllThingsSmitty/pen/JJavZN -->

<div class="coming-soon--countdown">
    <div class="days"><span id="days"></span> <br>Dagen</div>
</div>

<div class="coming-soon--countdown">
    <div class="hours"><span id="hours"></span> Uren</div>
    <div class="minutes"><span id="minutes"></span> Minuten</div>
    <div class="seconds"><span id="seconds"></span> Seconden</div>
</div>
